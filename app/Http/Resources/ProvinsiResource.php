<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProvinsiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'provinsi_id' => $this->provinsi_id,
            'provinsi_nama' => $this->provinsi_nama,
            'kabupaten' => $this->kabupaten
        ];
    }

    public function test()
    {
        return \response(['nama' => 'rahma']);
    }
}
