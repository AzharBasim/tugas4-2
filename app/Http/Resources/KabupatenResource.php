<?php

namespace App\Http\Resources;

use App\Kabupaten;
use Illuminate\Http\Resources\Json\JsonResource;

class KabupatenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'kabupaten_id' => $this->kabupaten_id,
          'kabupaten_nama' => $this->kabupaten_nama,
          'fk_provinsi' => $this->fk_provinsi,
          'provinsi' => $this->provinsi,
          'kecamatan' => $this->kecamatan
        ];
    }
}
