<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PenontonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'penonton_id' => $this->penonton_id,
          'penonton_nama' => $this->penonton_nama,
          'created_at' => $this->created_at,
          'updated_at' => $this->updated_at,
          'film' => $this->film
        ];
    }
}
