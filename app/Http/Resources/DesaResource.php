<?php

namespace App\Http\Resources;

use App\Kabupaten;
use Illuminate\Http\Resources\Json\JsonResource;

class DesaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'desa_id' => $this->desa_id,
          'desa_nama' => $this->desa_nama,
          'fk_kecamatan' => $this->fk_kecamatan,
          'kecamatan' => $this->kecamatan
        ];
    }
}
