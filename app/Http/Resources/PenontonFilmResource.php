<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PenontonFilmResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'fk_film' => $this->fk_film,
            'fk_penonton' => $this->fk_penonton,
            'penonton_film_id' => $this->penonton_film_id
        ];
    }
}
