<?php

namespace App\Http\Resources;

use App\Kabupaten;
use App\Kecamatan;
use App\Provinsi;
use Illuminate\Http\Resources\Json\JsonResource;

class KecamatanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'kecamatan_id' => $this->kecamatan_id,
            'kecamatan_nama' => $this->kecamatan_nama,
            'fk_kabupaten' => $this->fk_kabupaten,
            'kabupaten' => $this->kabupaten,
            'desa' => $this->desa
        ];
    }
}
