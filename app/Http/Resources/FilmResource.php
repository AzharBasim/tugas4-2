<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FilmResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'film_id' => $this->film_id,
            'film_nama' => $this->film_nama,
            'film_jam_tayang' => $this->film_jam_tayang,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'penonton' => $this->penonton
        ];
    }
}
