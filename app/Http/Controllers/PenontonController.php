<?php

namespace App\Http\Controllers;

use App\Http\Resources\PenontonResource;
use App\Penonton;
use Illuminate\Http\Request;

class PenontonController extends Controller
{
    public function index()
    {
        return PenontonResource::collection(Penonton::with('film')->paginate(25));
    }

    public function store(Request $request)
    {
        $penonton = Penonton::create([
            'penonton_nama' => ucwords($request->penonton_nama)
        ]);

        return new PenontonResource($penonton);
    }

    public function show(Penonton $penonton)
    {
        return new PenontonResource($penonton);
    }

    public function update(Request $request, Penonton $penonton)
    {
        $penonton->update($request->only(['penonton_nama']));

        return new PenontonResource($penonton);
    }

    public function destroy(Penonton $penonton)
    {
        $penonton->delete();

        return response()->noContent();
    }
}
