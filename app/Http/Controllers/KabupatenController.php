<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Provinsi;
use App\Kabupaten;
use App\Http\Resources\ProvinsiResource;
use App\Http\Resources\KabupatenResource;

class KabupatenController extends Controller
{
    public function index()
    {
        return KabupatenResource::collection(Kabupaten::with(['kecamatan.desa'])->paginate(25));
    }

    public function store(Request $request)
    {
        $prov = Kabupaten::create([
            'kabupaten_nama' => ucwords($request->kabupaten_nama),
            'fk_provinsi' => $request->fk_provinsi
        ]);

        return new KabupatenResource($prov);
    }

    public function show(Kabupaten $kabupaten)
    {
        return new KabupatenResource($kabupaten);
    }

    public function update(Request $request, Kabupaten $kabupaten)
    {
        $kabupaten->update($request->only(['kabupaten_nama']));

        return new KabupatenResource($kabupaten);
    }

    public function destroy(Kabupaten $kabupaten)
    {
        $kabupaten->delete();

        return response()->noContent();
    }
}
