<?php

namespace App\Http\Controllers;

use App\Http\Resources\PenontonFilmResource;
use App\Penonton;
use App\Penonton_Film;
use Illuminate\Http\Request;

class PenontonFilmController extends Controller
{
    public function deleteFilmOnPenonton($fk_penonton, $fk_film)
    {
        Penonton_Film::where('fk_penonton', $fk_penonton)
            ->where('fk_film', $fk_film)->delete();

        return response()->noContent();
    }

    public function store(Request $request)
    {
        $check = Penonton_Film::where('fk_film', $request->fk_film)
            ->where('fk_penonton', $request->fk_penonton)->first();
        if(!empty($check)){
            return response([], 208);
        }

        $penontonFilm = Penonton_Film::create([
            'fk_film' => $request->fk_film,
            'fk_penonton' => $request->fk_penonton
        ]);

        return new PenontonFilmResource($penontonFilm);
    }
}
