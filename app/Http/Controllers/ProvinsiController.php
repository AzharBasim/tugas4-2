<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Provinsi;
use App\Http\Resources\ProvinsiResource;

class ProvinsiController extends Controller
{
    public function index()
    {
        return ProvinsiResource::collection(Provinsi::with(['kabupaten.kecamatan.desa'])->paginate(25));
    }

    public function store(Request $request)
    {
        $prov = Provinsi::create([
            'provinsi_nama' => $request->provinsi_nama
        ]);

        return new ProvinsiResource($prov);
    }

    public function show(Provinsi $provinsi)
    {
        return new ProvinsiResource($provinsi);
    }

    public function update(Request $request, Provinsi $provinsi)
    {
        $provinsi->update($request->only(['provinsi_nama']));

        return new ProvinsiResource($provinsi);
    }

    public function destroy(Provinsi $provinsi)
    {
        $provinsi->delete();

        return response()->noContent();
    }
}
