<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Desa;
use App\Http\Resources\DesaResource;

class DesaController extends Controller
{
    public function index()
    {
        return DesaResource::collection(Desa::with(['kecamatan.kabupaten.provinsi'])->paginate(25));
    }

    public function store(Request $request)
    {
        $desa = Desa::create([
            'desa_nama' => ucwords($request->desa_nama),
            'fk_kecamatan' => $request->fk_kecamatan
        ]);

        return new DesaResource($desa);
    }

    public function show(Desa $desa)
    {

        return Desa::with(['kecamatan.kabupaten.provinsi'])
            ->whereKey($desa)->first();
    }

    public function update(Request $request, Desa $desa)
    {
        $desa->update($request->only(['desa_nama']));

        return new DesaResource($desa);
    }

    public function destroy(Desa $desa)
    {
        $desa->delete();

        return response()->noContent();
    }
}
