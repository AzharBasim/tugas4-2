<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kecamatan;
use App\Http\Resources\KecamatanResource;

class KecamatanController extends Controller
{
    public function index()
    {
        return KecamatanResource::collection(Kecamatan::with(['desa','kabupaten.provinsi'])->paginate(25));
    }

    public function store(Request $request)
    {
        $kec = Kecamatan::create([
            'kecamatan_nama' => ucwords($request->kecamatan_nama),
            'fk_kabupaten' => $request->fk_kabupaten
        ]);

        return new KecamatanResource($kec);
    }

    public function show(Kecamatan $kecamatan)
    {
        return new KecamatanResource($kecamatan);
    }

    public function update(Request $request, Kecamatan $kecamatan)
    {
        $kecamatan->update($request->only(['kecamatan_nama']));

        return new KecamatanResource($kecamatan);
    }

    public function destroy(Kecamatan $kecamatan)
    {
        $kecamatan->delete();

        return response()->noContent();
    }
}
