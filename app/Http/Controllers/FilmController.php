<?php

namespace App\Http\Controllers;

use App\Http\Resources\FilmResource;
use App\Film;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    public function index()
    {
        return FilmResource::collection(Film::with('penonton')->paginate(25));
    }

    public function store(Request $request)
    {
        $film = Film::create([
            'film_nama' => ucwords($request->film_nama),
            'film_jam_tayang' => $request->film_jam_tayang
        ]);

        return new FilmResource($film);
    }

    public function show(Film $film)
    {
        return new FilmResource($film);
    }

    public function update(Request $request, Film $film)
    {
        $film->update($request->only(['film_nama','film_jam_tayang']));

        return new FilmResource($film);
    }

    public function destroy(Film $film)
    {
        $film->delete();

        return response()->noContent();
    }
}
