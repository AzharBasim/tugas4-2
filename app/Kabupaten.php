<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    protected $table = 'kabupaten';
    protected $primaryKey = 'kabupaten_id';
    protected $fillable = ['kabupaten_nama','fk_provinsi'];
    protected $with = ['provinsi'];
    public $timestamps = false;

    public function provinsi()
    {
        return $this->belongsTo('App\Provinsi', 'fk_provinsi');
    }

    public function kecamatan()
    {
        return $this->hasMany('App\Kecamatan', 'fk_kabupaten');
    }
}
