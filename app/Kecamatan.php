<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'kecamatan';
    protected $primaryKey = 'kecamatan_id';
    protected $fillable = ['kecamatan_nama','fk_kabupaten'];
    public $timestamps = false;

    public function kabupaten()
    {
        return $this->belongsTo('App\Kabupaten', 'fk_kabupaten');
    }

    public function desa()
    {
        return $this->hasMany('App\Desa', 'fk_kecamatan');
    }
}
