<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penonton_Film extends Model
{
    protected $table = 'penonton_film';
    protected $primaryKey = 'penonton_film_id';
    protected $fillable = ['fk_penonton','fk_film'];
    public $timestamps = false;

//    public function penonton()
//    {
//        return $this->belongsTo('App\Penonton','fk_penonton');
//    }
//
//    public function film()
//    {
//        return $this->belongsTo('App\Film','fk_film');
//    }

}
