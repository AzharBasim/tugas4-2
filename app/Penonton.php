<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penonton extends Model
{
    protected $table = 'penonton';
    protected $primaryKey = 'penonton_id';
    protected $fillable = ['penonton_nama','created_at','updated_at'];

    public function film()
    {
        return $this->belongsToMany('App\Film','penonton_film','fk_penonton','fk_film');
    }
}
