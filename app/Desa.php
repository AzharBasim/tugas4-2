<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desa extends Model
{
    protected $table = 'desa';
    protected $primaryKey = 'desa_id';
    protected $fillable = ['desa_nama','fk_kecamatan'];
    public $timestamps = false;

    public function kecamatan()
    {
        return $this->belongsTo('App\Kecamatan', 'fk_kecamatan');
    }
}
