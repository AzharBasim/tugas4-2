<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = 'film';
    protected $primaryKey = 'film_id';
    protected $fillable = ['film_nama','film_jam_tayang','created_at','updated_at'];

    public function penonton()
    {
        return $this->belongsToMany(Penonton::class,'penonton_film','fk_film','fk_penonton');
    }
}
