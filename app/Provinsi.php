<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'provinsi';
    protected $primaryKey = 'provinsi_id';
    protected $fillable = ['provinsi_nama'];
    public $timestamps = false;

    public function kabupaten()
    {
        return $this->hasMany('App\Kabupaten', 'fk_provinsi');
    }
}
