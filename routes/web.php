<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/provinsi', 'app');
Route::view('/provinsi/create', 'app');
Route::view('/provinsi/edit/{provinsi_id}', 'app');
Route::view('/provinsi/{provinsi_id}', 'app');

Route::view('/kabupaten', 'app');
Route::view('/kabupaten/create', 'app');
Route::view('/kabupaten/edit/{kabupaten_id}', 'app');
Route::view('/kabupaten/{kabupaten_id}', 'app');

Route::view('/kecamatan', 'app');
Route::view('/kecamatan/create', 'app');
Route::view('/kecamatan/edit/{kecamatan_id}', 'app');
Route::view('/kecamatan/{kecamatan_id}', 'app');

Route::view('/desa', 'app');
Route::view('/desa/create', 'app');
Route::view('/desa/edit/{desa_id}', 'app');
Route::view('/desa/{desa_id}', 'app');

Route::view('/film', 'app');
Route::view('/film/create', 'app');
Route::view('/film/edit/{film_id}', 'app');
Route::view('/film/{film_id}', 'app');

Route::view('/penonton', 'app');
Route::view('/penonton/create', 'app');
Route::view('/penonton/edit/{penonton_id}', 'app');
Route::view('/penonton/{penonton_id}', 'app');
Route::view('/penonton-film/add', 'app');

Route::view('/', 'app');
Route::view('/{path}', 'app');
