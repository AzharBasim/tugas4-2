<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::apiResource('provinsi', 'ProvinsiController');
Route::apiResource('kabupaten', 'KabupatenController');
Route::apiResource('kecamatan', 'KecamatanController');
Route::apiResource('desa', 'DesaController');

Route::delete('penonton_film/{fk_penonton}/{fk_film}', 'PenontonFilmController@deleteFilmOnPenonton');
Route::post('penonton_film/create', 'PenontonFilmController@store');

Route::apiResource('film', 'FilmController');
Route::apiResource('penonton', 'PenontonController');
