import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Layouts/Header'
import ProvinsiIndex from './Provinsi/ProvinsiIndex'
import ProvinsiCreate from './Provinsi/ProvinsiCreate'
import ProvinsiShow from './Provinsi/ProvinsiShow'
import ProvinsiEdit from './Provinsi/ProvinsiEdit'

import KabupatenIndex from "./Kabupaten/KabupatenIndex";
import KabupatenCreate from "./Kabupaten/KabupatenCreate";
import KabupatenEdit from "./Kabupaten/KabupatenEdit";
import KabupatenShow from "./Kabupaten/KabupatenShow";

import KecamatanIndex from "./Kecamatan/KecamatanIndex";
import KecamatanCreate from "./Kecamatan/KecamatanCreate";
import KecamatanEdit from "./Kecamatan/KecamatanEdit";
import KecamatanShow from "./Kecamatan/KecamatanShow";

import DesaIndex from "./Desa/DesaIndex";
import DesaCreate from "./Desa/DesaCreate";
import DesaEdit from "./Desa/DesaEdit";
import DesaShow from "./Desa/DesaShow";

import PenontonIndex from './Penonton/PenontonIndex'
import PenontonCreate from './Penonton/PenontonCreate'
import PenontonShow from './Penonton/PenontonShow'
import PenontonEdit from './Penonton/PenontonEdit'
import PenontonFilmAdd from './Penonton/PenontonFilmAdd'

import FilmIndex from './Film/FilmIndex'
import FilmCreate from './Film/FilmCreate'
import FilmShow from './Film/FilmShow'
import FilmEdit from './Film/FilmEdit'
import PenontonFimAdd from "./Penonton/PenontonFilmAdd";

class App extends Component {
    render () {
        return (
            <BrowserRouter>
                <div>
                    <Header />
                    <Switch>
                        <Route exact path='/provinsi' component={ProvinsiIndex}/>
                        <Route exact path='/provinsi/create' component={ProvinsiCreate} />
                        <Route path='/provinsi/edit/:provinsi_id' component={ProvinsiEdit} />
                        <Route path='/provinsi/:provinsi_id' component={ProvinsiShow} />

                        <Route exact path='/kabupaten' component={KabupatenIndex}/>
                        <Route exact path='/kabupaten/create' component={KabupatenCreate} />
                        <Route path='/kabupaten/edit/:kabupaten_id' component={KabupatenEdit} />
                        <Route path='/kabupaten/:kabupaten_id' component={KabupatenShow} />

                        <Route exact path='/kecamatan' component={KecamatanIndex}/>
                        <Route exact path='/kecamatan/create' component={KecamatanCreate} />
                        <Route path='/kecamatan/edit/:kecamatan_id' component={KecamatanEdit} />
                        <Route path='/kecamatan/:kecamatan_id' component={KecamatanShow} />

                        <Route exact path='/desa' component={DesaIndex}/>
                        <Route exact path='/desa/create' component={DesaCreate} />
                        <Route path='/desa/edit/:desa_id' component={DesaEdit} />
                        <Route path='/desa/:desa_id' component={DesaShow} />

                        <Route exact path='/penonton' component={PenontonIndex}/>
                        <Route exact path='/penonton/create' component={PenontonCreate} />
                        <Route exact path='/penonton-film/add' component={PenontonFilmAdd} />
                        <Route path='/penonton/edit/:penonton_id' component={PenontonEdit} />
                        <Route path='/penonton/:penonton_id' component={PenontonShow} />

                        <Route exact path='/film' component={FilmIndex}/>
                        <Route exact path='/film/create' component={FilmCreate} />
                        <Route path='/film/edit/:film_id' component={FilmEdit} />
                        <Route path='/film/:film_id' component={FilmShow} />
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('app'))
