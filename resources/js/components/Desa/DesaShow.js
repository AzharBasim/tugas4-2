import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class DesaShow extends Component {
    constructor (props) {
        super(props)
        this.state = {
            desa_nama: '',
            provinsis: '',
            kecamatans: '',
            kabupatens: '',
        }
    }

    componentDidMount () {

        const desa_id = this.props.match.params.desa_id
        axios.get(`/api/desa/${desa_id}`).then(response => {
            console.log(response.data)
            this.setState({
                desa_nama: response.data.desa_nama,
                provinsis : response.data.kecamatan.kabupaten.provinsi,
                kabupatens : response.data.kecamatan.kabupaten,
                kecamatans : response.data.kecamatan
            })
        })
    }

    render () {
        const { desa_nama, provinsis, kecamatans } = this.state
        const { kabupatens } = this.state
        console.log(this.state)
        return (
            <>
                <div className='container py-4'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className='card'>
                                <div className='card-header bg-dark text-white'>Nama Desa: <b>{desa_nama}</b></div>
                                <div className='card-body'>
                                    <p> Provinsi : {provinsis?.provinsi_nama}</p>
                                    <p> Kabupaten : {kabupatens?.kabupaten_nama}</p>
                                    <p> Kecamatan : {kecamatans?.kecamatan_nama}</p>
                                    <Link
                                        className='btn btn-primary'
                                        to={`/desa`}
                                    >Back
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default DesaShow
