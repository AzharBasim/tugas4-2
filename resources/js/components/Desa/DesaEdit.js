import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

class DesaEdit extends Component {
    constructor (props) {
        super(props)
        this.state = {
            desa_nama: '',
            provinsis: '',
            kecamatans: '',
            kabupatens: '',
            alert: null,
            message:'',
            errors: []
        }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleUpdateArticle = this.handleUpdateArticle.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    componentDidMount () {

        const desa_id = this.props.match.params.desa_id
        axios.get(`/api/desa/${desa_id}`).then(response => {
            console.log(response.data)
            this.setState({
                desa_nama: response.data.desa_nama,
                provinsis : response.data.kecamatan.kabupaten.provinsi,
                kabupatens : response.data.kecamatan.kabupaten,
                kecamatans : response.data.kecamatan
            })
        })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                {this.state.message}
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess() {
        this.props.history.push('/desa');
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }

    handleUpdateArticle (event) {
        event.preventDefault()

        const article = {
            desa_nama: this.state.desa_nama
        }

        const desa_id = this.props.match.params.desa_id

        axios.put(`/api/desa/${desa_id}`, article)
            .then(response => {
                if(response.status === 200){
                    this.setState({
                        message: 'Berhasil di-Update!'
                    })
                    return this.goToHome();
                }

            });
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
              <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }

    render () {
        const { desa_nama, provinsis, kecamatans } = this.state
        const { kabupatens } = this.state
        console.log(this.state)
        console.log(kabupatens)
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <div className='card'>
                            <div className='card-header bg-dark text-white'>Edit Nama Desa</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleUpdateArticle}>
                                    <div className='form-group'>
                                        <label htmlFor='kecamatan_nama'>Nama Provinsi</label>
                                        <input
                                            id='provinsi_nama'
                                            type='text'
                                            disabled={true}
                                            className='form-control'
                                            name=''
                                            value={provinsis?.provinsi_nama || ''}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('kecamatan_nama')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='kecamatan_nama'>Nama Kabupaten</label>
                                        <input
                                            id='kebupaten_nama'
                                            type='text'
                                            disabled={true}
                                            className='form-control'
                                            name=''
                                            value={kabupatens?.kabupaten_nama || ''}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('kecamatan_nama')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='kecamatan_nama'>Nama Kecamatan</label>
                                        <input
                                            id='kecamatan_nama'
                                            type='text'
                                            disabled={true}
                                            className='form-control'
                                            name=''
                                            value={kecamatans?.kecamatan_nama || ''}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('kecamatan_nama')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='desa_nama'>Nama Desa</label>
                                        <input
                                            id='desa_nama'
                                            type='text'
                                            className={`form-control ${this.hasErrorFor('desa_nama') ? 'is-invalid' : ''}`}
                                            name='desa_nama'
                                            value={desa_nama || ''}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('desa_nama')}
                                    </div>
                                    <Link
                                        className='btn btn-secondary'
                                        to={`/desa`}
                                    >Back
                                    </Link>
                                    <button className='btn btn-primary'>Update</button>
                                    {this.state.alert}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default DesaEdit
