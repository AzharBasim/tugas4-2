import axios from 'axios'
import React, {Component, useState} from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';
import {Button, Modal} from "react-bootstrap";

class ProvinsiIndex extends Component {

    constructor () {
        super()
        this.state = {
            desas: [],
            msg: null,
            type: null,
            flash:false,
            alert: null,
        }
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }


    componentDidMount ()  {
        axios
            .get('/api/desa')
            .then( response => {
                this.setState({
                   desas: response.data.data
                })
            }).catch(error => {
            console.log(error);
        })
    }

    confirmDelete(id){
        const getAlert = () => (
            <SweetAlert
                warning
                showCancel
                confirmBtnText="Hapus"
                cancelBtnText="Batal"
                confirmBtnBsStyle="default"
                cancelBtnBsStyle="danger"
                title="Tunggu ..."
                onConfirm={() => this.deleteItem(id)}
                onCancel={() => this.hideAlert()}
                focusCancelBtn
            >
                Kalau udah dihapus, nggak bakal balik lagi.
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    deleteItem(desa_id) {
        axios.delete(`/api/desa/${desa_id}`,{
            desa_id:desa_id
        }).then(response => {
            console.log(response);
            if(response.status === 204){
                this.hideAlert();
                this.goToHome();
            }
        })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                Desa berhasil dihapus
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess(){
        this.componentDidMount();
        this.hideAlert();
    }

    render () {
        const { desas }  = this.state
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-8'>
                        <div className='card'>
                            <div className='card-header bg-dark text-white'>Semua Desa</div>
                            <div className='card-body'>
                                <Link className='btn btn-primary btn-sm mb-3' to='/desa/create'>
                                    Tambahkan Desa Baru
                                </Link>
                                <div className="table-responsive">
                                    <table className="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th width="50" className="text-center">#</th>
                                            <th>Nama Provinsi</th>
                                            <th>Nama Kabupaten</th>
                                            <th>Nama Kecamatan</th>
                                            <th>Nama Desa</th>
                                            <th width="200" className="text-center">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        { desas?.map((desa, i) => (
                                            <tr key={i}>
                                                <td width="50" className="text-center">{i + 1}</td>
                                                <td>{desa.kecamatan.kabupaten.provinsi.provinsi_nama}</td>
                                                <td>{desa.kecamatan.kabupaten.kabupaten_nama}</td>
                                                <td>{desa.kecamatan.kecamatan_nama}</td>
                                                <td>{desa.desa_nama}</td>
                                                <td width="200" className="text-center">
                                                    <div className="btn-group">
                                                        <Link
                                                            className='btn btn-primary btn-sm'
                                                            to={`/desa/${desa.desa_id}`}
                                                        >Detail
                                                        </Link>
                                                        <Link
                                                            className='btn btn-success btn-sm'
                                                            to={`/desa/edit/${desa.desa_id}`}
                                                        >Edit
                                                        </Link>
                                                        <button
                                                            className='btn btn-danger btn-sm'
                                                            onClick={() => this.confirmDelete(desa.desa_id)}
                                                        >Hapus
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        )) }
                                        </tbody>
                                    </table>
                                    {this.state.alert}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProvinsiIndex
