import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

class KabupatenCreate extends Component {

    constructor (props) {
        super(props)
        this.state = {
            nama: '',
            selectKec: '',
            kecamatans: null,
            alert: null,
            errors: []
        }

        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleCreateNewArticle = this.handleCreateNewArticle.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    componentDidMount () {
        axios.get(`/api/kecamatan`)
            .then(res => {
                console.log(res)
                this.setState({
                    kecamatans: res.data.data
                })
            })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                Berhasil Menambahkan Desa {this.state.nama}
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess() {
        this.props.history.push('/desa');
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }

    handleCreateNewArticle (event) {
        event.preventDefault()
        const dataKabCreate = {
            desa_nama : this.state.nama,
            fk_kecamatan : this.state.selectKec
        }
        axios.post('/api/desa', dataKabCreate).then(response => {
            if(response.status === 201){
                return this.goToHome();
            }
        })
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }

    render () {
        console.log(this.state.kecamatans)
        console.log(this.state.selectKec)
        const { kecamatans } = this.state
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-7'>
                        <div className='card'>
                            <div className='card-header'>Buat Desa Baru</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleCreateNewArticle}>
                                    <div className='form-group'>
                                        <label>Kecamatan</label>
                                        <select className='form-control' required={true} name="selectKec" onChange={this.handleFieldChange}>
                                            <option key='0' value=''>- Pilih -</option>
                                            {kecamatans?.map((kec, i) => (
                                                <option key={i} value={kec.kecamatan_id}>{kec.kecamatan_nama}</option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='nama'>Nama Desa</label>
                                        <input
                                            id='nama'
                                            type='text'
                                            className={`form-control ${this.hasErrorFor('nama') ? 'is-invalid' : ''}`}
                                            name='nama'
                                            value={this.state.nama}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('title')}
                                    </div>
                                    <Link
                                        className='btn btn-secondary'
                                        to={`/desa`}
                                    >Back
                                    </Link>
                                    <button className='btn btn-primary'>Tambahkan</button>
                                    {this.state.alert}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default KabupatenCreate
