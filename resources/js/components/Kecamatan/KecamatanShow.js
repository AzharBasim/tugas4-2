import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class KecamatanShow extends Component {
    constructor (props) {
        super(props)
        this.state = {
            kecamatan: {}
        }
    }

    componentDidMount () {

        const kecamatan_id = this.props.match.params.kecamatan_id

        axios.get(`/api/kecamatan/${kecamatan_id}`).then(response => {
            this.setState({
                kecamatan: response.data.data
            })
        })
    }

    render () {
        const { kecamatan } = this.state
        console.log(kecamatan)
        return (
            <>
                <div className='container py-4'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className='card'>
                                <div className='card-header'>Nama Kecamatan: <b>{kecamatan?.kecamatan_nama}</b></div>
                                <div className='card-body'>
                                    <p> Provinsi : {kecamatan?.kabupaten?.provinsi?.provinsi_nama}</p>
                                    <p> Kabupaten : {kecamatan?.kabupaten?.kabupaten_nama}</p>
                                    <Link
                                        className='btn btn-primary'
                                        to={`/kecamatan`}
                                    >Back
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='container py-4'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className='card'>
                                <div className='card-header bg-dark text-white'>Semua Desa pada Kecamatan <b>{kecamatan.kecamatan_nama}</b></div>
                                <div className='card-body'>
                                    <div className="table-responsive">
                                        <table className="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th width="50" className="text-center">#</th>
                                                <th>Nama Kecamatan</th>
                                                <th width="200" className="text-center">Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            { kecamatan.desa?.map((desa, i) => (
                                                <tr key={i}>
                                                    <td width="50" className="text-center">{i + 1}</td>
                                                    <td>{desa.desa_nama}</td>
                                                    <td width="200" className="text-center">
                                                        <div className="btn-group">
                                                            <Link
                                                                className='btn btn-primary btn-sm'
                                                                to={`/desa/${desa.desa_id}`}
                                                            >Detail
                                                            </Link>
                                                            <Link
                                                                className='btn btn-success btn-sm'
                                                                to={`/desa/edit/${desa.desa_id}`}
                                                            >Edit
                                                            </Link>
                                                            <button
                                                                className='btn btn-danger btn-sm'
                                                            >Hapus
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )) }
                                            </tbody>
                                        </table>
                                        {this.state.alert}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default KecamatanShow
