import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

class KecamatanEdit extends Component {
    constructor (props) {
        super(props)
        this.state = {
            kecamatan_nama: '',
            kabupaten: '',
            alert: null,
            message:'',
            errors: []
        }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleUpdateArticle = this.handleUpdateArticle.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    componentDidMount () {

        const kecamatan_id = this.props.match.params.kecamatan_id
        axios.get(`/api/kecamatan/${kecamatan_id}`).then(response => {
            console.log(response.data)
            this.setState({
                kecamatan_nama: response.data.data.kecamatan_nama,
                kabupaten : response.data.data.kabupaten
            })
        })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                {this.state.message}
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess() {
        this.props.history.push('/kecamatan');
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }

    handleUpdateArticle (event) {
        event.preventDefault()

        const article = {
            kecamatan_nama: this.state.kecamatan_nama
        }

        const kecamatan_id = this.props.match.params.kecamatan_id

        axios.put(`/api/kecamatan/${kecamatan_id}`, article)
            .then(response => {
                if(response.status === 200){
                    this.setState({
                        message: 'Berhasil diUpdate!'
                    })
                    return this.goToHome();
                }

            });
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
              <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }

    render () {
        const { kecamatan_nama, kabupaten } = this.state
        console.log(this.state)
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <div className='card'>
                            <div className='card-header'>Edit Nama Kecamatan</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleUpdateArticle}>
                                    <div className='form-group'>
                                        <label htmlFor='kecamatan_nama bg-dark text-white'>Nama Provinsi</label>
                                        <input
                                            id='provinsi_nama'
                                            type='text'
                                            disabled={true}
                                            className='form-control'
                                            name=''
                                            value={kabupaten?.provinsi?.provinsi_nama || ''}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('kecamatan_nama')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='kecamatan_nama'>Nama Kabupaten</label>
                                        <input
                                            id='kabupaten_nama'
                                            type='text'
                                            disabled={true}
                                            className='form-control'
                                            name=''
                                            value={kabupaten.kabupaten_nama || ''}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('kecamatan_nama')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='kecamatan_nama'>Nama Kecamatan</label>
                                        <input
                                            id='kecamatan_nama'
                                            type='text'
                                            className={`form-control ${this.hasErrorFor('kecamatan_nama') ? 'is-invalid' : ''}`}
                                            name='kecamatan_nama'
                                            value={kecamatan_nama || ''}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('kecamatan_nama')}
                                    </div>
                                    <Link
                                        className='btn btn-secondary'
                                        to={`/kecamatan`}
                                    >Back
                                    </Link>
                                    <button className='btn btn-primary'>Update</button>
                                    {this.state.alert}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default KecamatanEdit
