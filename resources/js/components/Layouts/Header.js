import React from 'react'
import {Button, Nav, Navbar} from 'react-bootstrap'
import { Link } from 'react-router-dom'

const Header = () => (
    <>
    <Navbar bg="dark" variant="dark" expand="lg">
        <Navbar.Brand href="/">{ document.title }</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
    </Navbar>
    <div className='container py-4'>
        <div className='card-columns d-flex justify-content-center'>
            <div className='col-md-5'>
                <div className='card'>
                    <div className='card-header bg-dark text-white'>Menu</div>
                    <div className='card-body text-center'>
                        <Link to={'/provinsi'} className='btn btn-primary m-2'>Provinsi</Link>
                        <Link to={'/kabupaten'} className='btn btn-info m-2'>Kabupaten</Link>
                        <Link to={'/kecamatan'} className='btn btn-warning m-2'>Kecamatan</Link>
                        <Link to={'/desa'} className='btn btn-danger m-2'>Desa</Link>
                    </div>
                </div>
            </div>
            <div className='col-md-5'>
                <div className='card'>
                <div className='card-header bg-dark text-white'>Menu Bioskop</div>
                        <div className='card-body text-center'>
                            <Link to={'/penonton'} className='btn btn-warning m-2'>Penonton</Link>
                            <Link to={'/film'} className='btn btn-success m-2'>Film</Link>
                        </div>
                </div>
            </div>
        </div>
    </div>
    </>
)

export default Header
