import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

class PenontonCreate extends Component {

    constructor (props) {
        super(props)
        this.state = {
            nama: '',
            alert: null,
            errors: []
        }

        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleCreateNewPenonton = this.handleCreateNewPenonton.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                Berhasil Menambahkan Penonton {this.state.nama}
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess() {
        this.props.history.push('/penonton');
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }

    handleCreateNewPenonton (event) {
        event.preventDefault()
        const penontonDataCreate = {
            penonton_nama : this.state.nama,

        }
        axios.post('/api/penonton', penontonDataCreate).then(response => {
            if(response.status === 201){
                return this.goToHome();
            }
        })
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }

    render () {
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <div className='card'>
                            <div className='card-header bg-dark text-white'>Buat Penonton Baru</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleCreateNewPenonton}>
                                    <div className='form-group'>
                                        <label htmlFor='nama'>Nama Penonton</label>
                                        <input
                                            id='nama'
                                            type='text'
                                            className={`form-control ${this.hasErrorFor('nama') ? 'is-invalid' : ''}`}
                                            name='nama'
                                            value={this.state.nama}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('nama')}
                                    </div>
                                    <Link
                                        className='btn btn-secondary'
                                        to={`/penonton`}
                                    >Back
                                    </Link>
                                    <button className='btn btn-primary'>Tambahkan</button>
                                    {this.state.alert}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default PenontonCreate
