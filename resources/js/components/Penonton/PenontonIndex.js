import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

class PenontonIndex extends Component {

    constructor () {
        super()
        this.state = {
            penontons: [],
            msg: null,
            type: null,
            flash:false,
            alert: null,
        }
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }


    componentDidMount ()  {
        axios
            .get('/api/penonton')
            .then( response => {
                // console.log(response);
                this.setState({
                   penontons: response.data.data
                })
                // console.log(this.state.penontons)
            }).catch(error => {
            console.log(error);
        })
    }

    confirmDelete(id){
        const getAlert = () => (
            <SweetAlert
                warning
                showCancel
                confirmBtnText="Hapus"
                cancelBtnText="Batal"
                confirmBtnBsStyle="default"
                cancelBtnBsStyle="danger"
                title="Tunggu ..."
                onConfirm={() => this.deleteItem(id)}
                onCancel={() => this.hideAlert()}
                focusCancelBtn
            >
                Kalau udah dihapus, nggak bakal balik lagi.
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    deleteItem(penonton_id) {
        axios.delete(`/api/penonton/${penonton_id}`,{
            penonton_id:penonton_id
        }).then(response => {
            console.log(response);
            if(response.status === 204){
                this.hideAlert();
                this.goToHome();
            }
        })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                Penonton Berhasil Dihapus
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess(){
        this.componentDidMount();
        this.hideAlert();
    }


    render () {
        const { penontons }  = this.state
        console.log(penontons)
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-8'>
                        <div className='card'>
                            <div className='card-header bg-dark text-white'>Semua Penonton</div>
                            <div className='card-body'>
                                <Link className='btn btn-primary btn-sm mb-3 mr-2' to='/penonton/create'>
                                    Tambahkan Penonton Baru
                                </Link>
                                <Link className='btn btn-outline-info btn-sm mb-3' to='/penonton-film/add'>
                                    Tambahkan Penonton Menonton Film
                                </Link>
                                <div className="table-responsive">
                                    <table className="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th width="50" className="text-center">#</th>
                                            <th>Nama Penonton</th>
                                            <th>Dibuat</th>
                                            <th className={`text-center`}>Sudah Reservasi</th>
                                            <th width="200" className="text-center">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        { penontons?.map((penonton, i) => (
                                            <tr key={i}>
                                                <td width="50" className="text-center">{i + 1}</td>
                                                <td>{penonton.penonton_nama}</td>
                                                <td>{penonton.created_at.substr(0, 16).replace('T',' ')}</td>
                                                <td className={`text-center`}>{penonton.film.length !== 0 ?
                                                    <span className='badge badge-success'>Sudah</span> : <span className='badge badge-secondary'>Belum</span>}</td>
                                                <td width="200" className="text-center">
                                                    <div className="btn-group">
                                                        <Link
                                                            className='btn btn-primary btn-sm'
                                                            to={`/penonton/${penonton.penonton_id}`}
                                                        >Detail
                                                        </Link>
                                                        <Link
                                                            className='btn btn-success btn-sm'
                                                            to={`/penonton/edit/${penonton.penonton_id}`}
                                                        >Edit
                                                        </Link>
                                                        <button
                                                            className='btn btn-danger btn-sm'
                                                            onClick={() => this.confirmDelete(penonton.penonton_id)}
                                                        >Hapus
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        )) }
                                        </tbody>
                                    </table>
                                    {this.state.alert}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default PenontonIndex
