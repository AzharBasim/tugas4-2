import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from "react-bootstrap-sweetalert";

class PenontonShow extends Component {
    constructor (props) {
        super(props)
        this.state = {
            penonton: {}
        }
    }

    componentDidMount () {

        const penonton_id = this.props.match.params.penonton_id

        axios.get(`/api/penonton/${penonton_id}`).then(response => {
            this.setState({
                penonton: response.data.data
            })
        })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                Provinsi berhasil dihapus
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess(){
        this.componentDidMount();
        this.hideAlert();
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }

    confirmDeleteHasFilmPenonton(id, film){
        const getAlert = () => (
            <SweetAlert
                warning
                showCancel
                confirmBtnText="Hapus"
                cancelBtnText="Batal"
                confirmBtnBsStyle="default"
                cancelBtnBsStyle="danger"
                title="Tunggu ..."
                onConfirm={() => this.deleteItem(id, film)}
                onCancel={() => this.hideAlert()}
                focusCancelBtn
            >
                Kalau udah dihapus, nggak bakal balik lagi.
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    deleteItem(penonton_id, film_id) {
        axios.delete(`/api/penonton_film/${penonton_id}/${film_id}`,{
            fk_penonton:penonton_id,
            fk_film:film_id
        }).then(response => {
            console.log(response);
            if(response.status === 204){
                this.hideAlert();
                this.goToHome();
            }
        })
    }

    render () {
        const { penonton } = this.state
        console.log(penonton)
        return (
            <>
                <div className='container py-4'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className='card'>
                                <div className='card-header bg-dark text-white'>Nama Penonton: <b>{penonton?.penonton_nama}</b></div>
                                <div className='card-body'>
                                    <p>Nama : {penonton?.penonton_nama}</p>
                                    <p>Dibuat : {penonton.created_at?.substr(0,16).replace('T',' ')}</p>
                                    <p>Diupdate : {penonton.updated_at?.substr(0,16).replace('T',' ')}</p>
                                    <Link
                                        className='btn btn-primary'
                                        to={`/penonton`}
                                    >Back
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='container py-4'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className='card'>
                                <div className='card-header'>Semua Film yang di pesan Penonton <b>{penonton.penonton_nama}</b></div>
                                <div className='card-body'>
                                    <div className="table-responsive">
                                        <table className="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th width="50" className="text-center">#</th>
                                                <th>Nama Film</th>
                                                <th>Jam Tayang</th>
                                                <th>Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            { penonton.film?.map((film, i) => (
                                                <tr key={i}>
                                                    <td width="50" className="text-center">{i + 1}</td>
                                                    <td>{film.film_nama}</td>
                                                    <td>{film.film_jam_tayang.substr(0,16).replace('T',' ')}</td>
                                                    <td width="200" className="text-center">
                                                        <div className="btn-group">
                                                            <Link
                                                                className='btn btn-primary btn-sm'
                                                                to={`/film/${film.film_id}`}
                                                            >Detail
                                                            </Link>
                                                            <button
                                                                className='btn btn-danger btn-sm'
                                                                onClick={() => this.confirmDeleteHasFilmPenonton(film.pivot.fk_penonton,film.pivot.fk_film)}
                                                            >Hapus
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )) }
                                            </tbody>
                                        </table>
                                        {this.state.alert}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default PenontonShow
