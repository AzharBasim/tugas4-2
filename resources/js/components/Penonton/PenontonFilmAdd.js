import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

class PenontonFilmAdd extends Component {

    constructor (props) {
        super(props)
        this.state = {
            penontons: [],
            films: [],
            selectFilm: '',
            selectPenonton: '',
            alert: null,
            errors: []
        }

        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleCreateNewPenonton = this.handleCreateNewPenonton.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                Berhasil Menambahkan Penonton {this.state.nama}
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    goToCurrentPage(){
        const getAlert = () => (
            <SweetAlert
                info
                title="Upss!"
                onConfirm={() => this.onFailed() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                Penonton sudah Menonton Film tersebut.
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess() {
        this.props.history.push('/penonton');
    }

    onFailed(){
        this.hideAlert();
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }

    componentDidMount() {
        axios.get('/api/penonton')
            .then((response) => {
                this.setState({
                    penontons : response.data.data
                })
            })

        axios.get('/api/film')
            .then(response => {
                this.setState({
                    films : response.data.data
                })
            })
    }

    handleCreateNewPenonton (event) {
        event.preventDefault()
        const penontonDataCreate = {
            fk_film : this.state.selectFilm,
            fk_penonton : this.state.selectPenonton

        }
        axios.post('/api/penonton_film/create', penontonDataCreate).then(response => {
            if(response.status === 201){
                return this.goToHome();
            } else {
                return this.goToCurrentPage();
            }
        })
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }

    render () {
        console.log(this.state);
        const { films, penontons } = this.state
        console.log(penontons);
        console.log(films);
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <div className='card'>
                            <div className='card-header bg-dark text-white'>Buat Penonton Baru</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleCreateNewPenonton}>
                                    <div className='form-group'>
                                        <label>Film</label>
                                        <select className='form-control' required={true} name="selectFilm" onChange={this.handleFieldChange}>
                                            <option key='0' value=''>- Pilih -</option>
                                            {films?.map((fil, i) => (
                                                <option key={i} value={fil.film_id}>{fil.film_nama} - {fil.film_jam_tayang}</option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className='form-group'>
                                        <label>Penonton</label>
                                        <select className='form-control' required={true} name="selectPenonton" onChange={this.handleFieldChange}>
                                            <option key='0' value=''>- Pilih -</option>
                                            {penontons?.map((pen, i) => (
                                                <option key={i} value={pen.penonton_id}>{pen.penonton_nama}</option>
                                            ))}
                                        </select>
                                    </div>
                                    <Link
                                        className='btn btn-secondary'
                                        to={`/penonton`}
                                    >Back
                                    </Link>
                                    <button className='btn btn-primary'>Tambahkan</button>
                                    {this.state.alert}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default PenontonFilmAdd
