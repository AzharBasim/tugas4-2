import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

class FilmIndex extends Component {

    constructor () {
        super()
        this.state = {
            films: [],
            msg: null,
            type: null,
            flash:false,
            alert: null,
        }
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }


    componentDidMount ()  {
        axios
            .get('/api/film')
            .then( response => {
                this.setState({
                   films: response.data.data
                })
            }).catch(error => {
            console.log(error);
        })
    }

    confirmDelete(id){
        const getAlert = () => (
            <SweetAlert
                warning
                showCancel
                confirmBtnText="Hapus"
                cancelBtnText="Batal"
                confirmBtnBsStyle="default"
                cancelBtnBsStyle="danger"
                title="Tunggu ..."
                onConfirm={() => this.deleteItem(id)}
                onCancel={() => this.hideAlert()}
                focusCancelBtn
            >
                Kalau udah dihapus, nggak bakal balik lagi.
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    deleteItem(film_id) {
        axios.delete(`/api/film/${film_id}`,{
            film_id:film_id
        }).then(response => {
            console.log(response);
            if(response.status === 204){
                this.hideAlert();
                this.goToHome();
            }
        })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                Film Berhasil Dihapus
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess(){
        this.componentDidMount();
        this.hideAlert();
    }


    render () {
        const { films }  = this.state
        console.log(films)
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-8'>
                        <div className='card'>
                            <div className='card-header bg-dark text-white'>Semua Film</div>
                            <div className='card-body'>
                                <Link className='btn btn-primary btn-sm mb-3' to='/film/create'>
                                    Tambahkan Film Baru
                                </Link>
                                <div className="table-responsive">
                                    <table className="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th width="50" className="text-center">#</th>
                                            <th>Nama Film</th>
                                            <th>Jam Tayang</th>
                                            <th>Dibuat</th>
                                            <th width="200" className="text-center">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        { films?.map((film, i) => (
                                            <tr key={i}>
                                                <td width="50" className="text-center">{i + 1}</td>
                                                <td>{film.film_nama}</td>
                                                <td>{film.film_jam_tayang.substr(0,16).replace('T', ' ')}</td>
                                                <td>{film.created_at.substr(0,16).replace('T', ' ')}</td>
                                                <td width="200" className="text-center">
                                                    <div className="btn-group">
                                                        <Link
                                                            className='btn btn-primary btn-sm'
                                                            to={`/film/${film.film_id}`}
                                                        >Detail
                                                        </Link>
                                                        <Link
                                                            className='btn btn-success btn-sm'
                                                            to={`/film/edit/${film.film_id}`}
                                                        >Edit
                                                        </Link>
                                                        <button
                                                            className='btn btn-danger btn-sm'
                                                            onClick={() => this.confirmDelete(film.film_id)}
                                                        >Hapus
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        )) }
                                        </tbody>
                                    </table>
                                    {this.state.alert}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default FilmIndex
