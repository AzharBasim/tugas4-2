import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

class FilmEdit extends Component {
    constructor (props) {
        super(props)
        this.state = {
            film: [],
            film_nama: '',
            jam_tayang: '',
            hari_tayang: '',
            alert: null,
            message:'',
            errors: []
        }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleUpdateArticle = this.handleUpdateArticle.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    componentDidMount () {

        const film_id = this.props.match.params.film_id
        axios.get(`/api/film/${film_id}`).then(response => {
            console.log(response.data)
            this.setState({
                film: response.data.data,
            })
        })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                {this.state.message}
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess() {
        this.props.history.push('/film');
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }

    handleUpdateArticle (event) {
        event.preventDefault()

        const filmUpdate = {
            'film_nama': this.state.film_nama,
            'film_jam_tayang' : `${this.state.hari_tayang} ${this.state.jam_tayang}`
        }
        console.log(filmUpdate)
        const film_id = this.props.match.params.film_id

        axios.put(`/api/film/${film_id}`, filmUpdate)
            .then(response => {
                if(response.status === 200){
                    this.setState({
                        message: 'Berhasil Update!'
                    })
                    return this.goToHome();
                }

            });
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
              <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }

    render () {
        const { film } = this.state
        console.log(film)
        console.log(film?.film_jam_tayang?.substr(11,16))
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <div className='card'>
                            <div className='card-header bg-dark text-white'>Edit Data Film</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleUpdateArticle}>
                                    <div className='form-group'>
                                        <label htmlFor='film_nama'>Nama Film</label>
                                        <input
                                            id='film_nama'
                                            type='text'
                                            className={`form-control ${this.hasErrorFor('film_nama') ? 'is-invalid' : ''}`}
                                            name='film_nama'
                                            value={this.state.film_nama || film?.film_nama || ''}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('film_nama')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='hari_tayang'>Hari Tayang</label>
                                        <input
                                            id='hari_tayang'
                                            required={true}
                                            type='date'
                                            className={`form-control ${this.hasErrorFor('hari_tayang') ? 'is-invalid' : ''}`}
                                            name='hari_tayang'
                                            value={film?.film_jam_tayang?.substr(0,10) || ''}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('hari_tayang')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='jam_tayang'>Jam Tayang</label>
                                        <input
                                            id='jam_tayang'
                                            required={true}
                                            type='time'
                                            className={`form-control ${this.hasErrorFor('jam_tayang') ? 'is-invalid' : ''}`}
                                            name='jam_tayang'
                                            value={film?.film_jam_tayang?.substr(11,16) || this.state.jam_tayang || ''}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('jam_tayang')}
                                    </div>
                                    <Link
                                        className='btn btn-secondary'
                                        to={`/film`}
                                    >Back
                                    </Link>
                                    <button className='btn btn-primary'>Update</button>
                                    {this.state.alert}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default FilmEdit
