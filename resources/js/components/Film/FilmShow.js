import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class FilmShow extends Component {
    constructor (props) {
        super(props)
        this.state = {
            film: {}
        }
    }

    componentDidMount () {

        const film_id = this.props.match.params.film_id

        axios.get(`/api/film/${film_id}`).then(response => {
            this.setState({
                film: response.data.data
            })
        })
    }

    render () {
        const { film } = this.state
        console.log(film)
        return (
            <>
                <div className='container py-4'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className='card'>
                                <div className='card-header'>Nama Film: <b>{film.film_nama}</b></div>
                                <div className='card-body'>
                                    <p>Nama Film : {film.film_nama}</p>
                                    <p>Jam Tayang : {film?.film_jam_tayang?.substr(0,16).replace('T', ' ')}</p>
                                    <p>Dibuat : {film?.created_at?.substr(0,16).replace('T', ' ')}</p>
                                    <Link
                                        className='btn btn-primary'
                                        to={`/film`}
                                    >Back
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='container py-4'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className='card'>
                                <div className='card-header bg-dark text-white'>Semua Penonton pada Film <b>{film.film_nama}</b></div>
                                <div className='card-body'>
                                    <div className="table-responsive">
                                        <table className="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th width="50" className="text-center">#</th>
                                                <th>Nama Penonton</th>
                                                <th className={`text-center`}>Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            { film.penonton?.map((penonton, i) => (
                                                <tr key={i}>
                                                    <td width="50" className="text-center">{i + 1}</td>
                                                    <td>{penonton.penonton_nama}</td>
                                                    <td width="200" className="text-center">
                                                        <div className="btn-group">
                                                            <Link
                                                                className='btn btn-primary btn-sm'
                                                                to={`/penonton/${penonton.penonton_id}`}
                                                            >Detail
                                                            </Link>
                                                            <Link
                                                                className='btn btn-success btn-sm'
                                                                to={`/penonton/edit/${penonton.penonton_id}`}
                                                            >Edit
                                                            </Link>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )) }
                                            </tbody>
                                        </table>
                                        {this.state.alert}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default FilmShow
