import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

class FilmCreate extends Component {

    constructor (props) {
        super(props)
        this.state = {
            nama: '',
            jam_tayang: '',
            hari_tayang: '',
            alert: null,
            errors: []
        }

        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleCreateNewArticle = this.handleCreateNewArticle.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                Berhasil Menambahkan Film {this.state.nama}
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess() {
        this.props.history.push('/film');
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }

    handleCreateNewArticle (event) {
        event.preventDefault()
        const filmCreate = {
            film_nama : this.state.nama,
            film_jam_tayang : `${this.state.hari_tayang} ${this.state.jam_tayang}`
        }
        axios.post('/api/film', filmCreate).then(response => {
            if(response.status === 201){
                return this.goToHome();
            }
        })
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }

    render () {
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <div className='card'>
                            <div className='card-header bg-dark text-white'>Tambahkan Film Baru</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleCreateNewArticle}>
                                    <div className='form-group'>
                                        <label htmlFor='nama'>Nama Film</label>
                                        <input
                                            id='nama'
                                            type='text'
                                            required={true}
                                            className={`form-control ${this.hasErrorFor('nama') ? 'is-invalid' : ''}`}
                                            name='nama'
                                            value={this.state.nama}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('nama')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='nama'>Tanggal Tayang</label>
                                        <input
                                            id='hari_tayang'
                                            required={true}
                                            type='date'
                                            className={`form-control ${this.hasErrorFor('hari_tayang') ? 'is-invalid' : ''}`}
                                            name='hari_tayang'
                                            value={this.state.hari_tayang}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('hari_tayang')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='nama'>Jam Tayang</label>
                                        <input
                                            id='jam_tayang'
                                            required={true}
                                            type='time'
                                            className={`form-control ${this.hasErrorFor('jam_tayang') ? 'is-invalid' : ''}`}
                                            name='jam_tayang'
                                            value={this.state.jam_tayang}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('jam_tayang')}
                                    </div>
                                    <Link
                                        className='btn btn-secondary'
                                        to={`/film`}
                                    >Back
                                    </Link>
                                    <button className='btn btn-primary'>Tambahkan</button>
                                    {this.state.alert}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default FilmCreate
