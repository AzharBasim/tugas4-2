import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

class KabupatenIndex extends Component {

    constructor () {
        super()
        this.state = {
            kabupatens: [],
            msg: null,
            type: null,
            flash:false,
            alert: null,
        }
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }


    componentDidMount ()  {
        axios
            .get('/api/kabupaten')
            .then( response => {
                this.setState({
                   kabupatens: response.data.data
                })
            }).catch(error => {
            console.log(error);
        })
    }

    confirmDelete(id){
        const getAlert = () => (
            <SweetAlert
                warning
                showCancel
                confirmBtnText="Hapus"
                cancelBtnText="Batal"
                confirmBtnBsStyle="default"
                cancelBtnBsStyle="danger"
                title="Tunggu ..."
                onConfirm={() => this.deleteItem(id)}
                onCancel={() => this.hideAlert()}
                focusCancelBtn
            >
                Kalau udah dihapus, nggak bakal balik lagi.
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    deleteItem(kabupaten_id) {
        axios.delete(`/api/kabupaten/${kabupaten_id}`,{
            kabupaten_id:kabupaten_id
        }).then(response => {
            console.log(response);
            if(response.status === 204){
                this.hideAlert();
                this.goToHome();
            }
        })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                Kabupaten Berhasil dihapus
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess(){
        this.componentDidMount();
        this.hideAlert();
    }

    render () {
        const { kabupatens }  = this.state
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-8'>
                        <div className='card'>
                            <div className='card-header bg-dark text-white'>Semua Kabupaten</div>
                            <div className='card-body'>
                                <Link className='btn btn-primary btn-sm mb-3' to='/kabupaten/create'>
                                    Tambahkan Kabupaten Baru
                                </Link>
                                <div className="table-responsive">
                                    <table className="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th width="50" className="text-center">#</th>
                                            <th>Nama Provinsi</th>
                                            <th>Nama Kabupaten</th>
                                            <th width="200" className="text-center">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        { kabupatens?.map((kabupaten, i) => (
                                            <tr key={i}>
                                                <td width="50" className="text-center">{i + 1}</td>
                                                <td>{kabupaten.provinsi.provinsi_nama}</td>
                                                <td>{kabupaten.kabupaten_nama}</td>
                                                <td width="200" className="text-center">
                                                    <div className="btn-group">
                                                        <Link
                                                            className='btn btn-primary btn-sm'
                                                            to={`/kabupaten/${kabupaten.kabupaten_id}`}
                                                        >Detail
                                                        </Link>
                                                        <Link
                                                            className='btn btn-success btn-sm'
                                                            to={`/kabupaten/edit/${kabupaten.kabupaten_id}`}
                                                        >Edit
                                                        </Link>
                                                        <button
                                                            className='btn btn-danger btn-sm'
                                                            onClick={() => this.confirmDelete(kabupaten.kabupaten_id)}
                                                        >Hapus
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        )) }
                                        </tbody>
                                    </table>
                                    {this.state.alert}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default KabupatenIndex
