import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

class KabupatenEdit extends Component {
    constructor (props) {
        super(props)
        this.state = {
            kabupaten_nama: '',
            provinsi: '',
            alert: null,
            message:'',
            errors: []
        }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleUpdateArticle = this.handleUpdateArticle.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    componentDidMount () {

        const kabupaten_id = this.props.match.params.kabupaten_id
        axios.get(`/api/kabupaten/${kabupaten_id}`).then(response => {
            console.log(response.data)
            this.setState({
                kabupaten_nama: response.data.data.kabupaten_nama,
                provinsi : response.data.data.provinsi
            })
        })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                {this.state.message}
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess() {
        this.props.history.push('/kabupaten');
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }

    handleUpdateArticle (event) {
        event.preventDefault()

        const article = {
            kabupaten_nama: this.state.kabupaten_nama
        }

        const kabupaten_id = this.props.match.params.kabupaten_id

        axios.put(`/api/kabupaten/${kabupaten_id}`, article)
            .then(response => {
                if(response.status === 200){
                    this.setState({
                        message: 'Berhasil diUpdate!'
                    })
                    return this.goToHome();
                }

            });
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
              <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }

    render () {
        const { kabupaten_nama, provinsi } = this.state
        console.log(this.state)
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <div className='card'>
                            <div className='card-header bg-dark text-white'>Edit Nama Kabupaten</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleUpdateArticle}>
                                    <div className='form-group'>
                                        <label htmlFor='kabupaten_nama'>Nama Kabupaten</label>
                                        <input
                                            id='kabupaten_nama'
                                            type='text'
                                            disabled={true}
                                            className='form-control'
                                            name=''
                                            value={provinsi.provinsi_nama || ''}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('kabupaten_nama')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='kabupaten_nama'>Nama Kabupaten</label>
                                        <input
                                            id='kabupaten_nama'
                                            type='text'
                                            className={`form-control ${this.hasErrorFor('kabupaten_nama') ? 'is-invalid' : ''}`}
                                            name='kabupaten_nama'
                                            value={kabupaten_nama || ''}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('kabupaten_nama')}
                                    </div>
                                    <Link
                                        className='btn btn-secondary'
                                        to={`/kabupaten`}
                                    >Back
                                    </Link>
                                    <button className='btn btn-primary'>Update</button>
                                    {this.state.alert}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default KabupatenEdit
