import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class KabupatenShow extends Component {
    constructor (props) {
        super(props)
        this.state = {
            kabupaten: {}
        }
    }

    componentDidMount () {

        const kabupaten_id = this.props.match.params.kabupaten_id

        axios.get(`/api/kabupaten/${kabupaten_id}`).then(response => {
            this.setState({
                kabupaten: response.data.data
            })
        })
    }

    render () {
        const { kabupaten } = this.state
        const provinsi = this.state.kabupaten.provinsi
        console.log(this.state)
        return (
            <>
                <div className='container py-4'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className='card'>
                                <div className='card-header'>Nama Kabupaten: <b>{kabupaten?.kabupaten_nama}</b></div>
                                <div className='card-body'>
                                    <p> Provinsi : {provinsi?.provinsi_nama}</p>
                                    <p> Kabupaten : {kabupaten?.kabupaten_nama}</p>
                                    <Link
                                        className='btn btn-primary'
                                        to={`/kabupaten`}
                                    >Back
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='container py-4'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className='card'>
                                <div className='card-header bg-dark text-white'>Semua Kecamatan pada Kabupaten <b>{kabupaten.kabupaten_nama}</b></div>
                                <div className='card-body'>
                                    {/*<Link className='btn btn-primary btn-sm mb-3' to='/kabupaten/create'>*/}
                                    {/*    Tambahkan Kecamatan Baru untuk <b>{kabupaten.kabupaten_nama}</b>*/}
                                    {/*</Link>*/}
                                    <div className="table-responsive">
                                        <table className="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th width="50" className="text-center">#</th>
                                                <th>Nama Kecamatan</th>
                                                <th width="200" className="text-center">Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            { kabupaten.kecamatan?.map((kecamatan, i) => (
                                                <tr key={i}>
                                                    <td width="50" className="text-center">{i + 1}</td>
                                                    <td>{kecamatan.kecamatan_nama}</td>
                                                    <td width="200" className="text-center">
                                                        <div className="btn-group">
                                                            <Link
                                                                className='btn btn-primary btn-sm'
                                                                to={`/kecamatan/${kecamatan.kecamatan_id}`}
                                                            >Detail
                                                            </Link>
                                                            <Link
                                                                className='btn btn-success btn-sm'
                                                                to={`/kecamatan/edit/${kecamatan.kecamatan_id}`}
                                                            >Edit
                                                            </Link>
                                                            <button
                                                                className='btn btn-danger btn-sm'
                                                            >Hapus
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )) }
                                            </tbody>
                                        </table>
                                        {this.state.alert}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default KabupatenShow
