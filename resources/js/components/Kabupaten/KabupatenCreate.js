import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

class KabupatenCreate extends Component {

    constructor (props) {
        super(props)
        this.state = {
            nama: '',
            selectProv: '',
            provinsis: null,
            alert: null,
            errors: []
        }

        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleCreateNewArticle = this.handleCreateNewArticle.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    componentDidMount () {
        axios.get(`/api/provinsi`)
            .then(res => {
                console.log(res)
                this.setState({
                    provinsis: res.data.data
                })
            })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                Berhasil Menambahkan Kabupaten {this.state.nama}
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess() {
        this.props.history.push('/kabupaten');
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }

    handleCreateNewArticle (event) {
        event.preventDefault()
        const dataKabCreate = {
            kabupaten_nama : this.state.nama,
            fk_provinsi : this.state.selectProv
        }
        axios.post('/api/kabupaten', dataKabCreate).then(response => {
            if(response.status === 201){
                return this.goToHome();
            }
        })
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }

    render () {
        console.log(this.state.provinsis)
        console.log(this.state.selectProv)
        const { provinsis } = this.state
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <div className='card'>
                            <div className='card-header bg-dark text-white'>Buat Kabupaten Baru</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleCreateNewArticle}>
                                    <div className='form-group'>
                                        <label>Provinsi</label>
                                        <select required={true} className='form-control' name="selectProv" onChange={this.handleFieldChange}>
                                            <option key='0' value=''>- Pilih -</option>
                                            {provinsis?.map((prov, i) => (
                                                <option key={i} value={prov.provinsi_id}>{prov.provinsi_nama}</option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='nama'>Nama Kabupaten</label>
                                        <input
                                            id='nama'
                                            type='text'
                                            className={`form-control ${this.hasErrorFor('nama') ? 'is-invalid' : ''}`}
                                            name='nama'
                                            value={this.state.nama}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('title')}
                                    </div>
                                    <Link
                                        className='btn btn-secondary'
                                        to={`/kabupaten`}
                                    >Back
                                    </Link>
                                    <button className='btn btn-primary'>Tambahkan</button>
                                    {this.state.alert}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default KabupatenCreate
