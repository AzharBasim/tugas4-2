import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

class ProvinsiEdit extends Component {
    constructor (props) {
        super(props)
        this.state = {
            provinsi_nama: '',
            alert: null,
            message:'',
            errors: []
        }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleUpdateArticle = this.handleUpdateArticle.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    componentDidMount () {

        const provinsi_id = this.props.match.params.provinsi_id
        axios.get(`/api/provinsi/${provinsi_id}`).then(response => {
            console.log(response.data)
            this.setState({
                provinsi_nama: response.data.data.provinsi_nama,
            })
        })
    }

    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke"
            >
                {this.state.message}
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }

    onSuccess() {
        this.props.history.push('/provinsi');
    }

    hideAlert() {
        this.setState({
            alert: null
        });
    }

    handleUpdateArticle (event) {
        event.preventDefault()

        const article = {
            provinsi_nama: this.state.provinsi_nama
        }

        const provinsi_id = this.props.match.params.provinsi_id

        axios.put(`/api/provinsi/${provinsi_id}`, article)
            .then(response => {
                if(response.status === 200){
                    this.setState({
                        message: 'Berhasil diUpdate!'
                    })
                    return this.goToHome();
                }

            });
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
              <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }

    render () {
        const { provinsi_nama } = this.state
        console.log(this.state)
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <div className='card'>
                            <div className='card-header bg-dark text-white'>Edit Nama Provinsi</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleUpdateArticle}>
                                    <div className='form-group'>
                                        <label htmlFor='provinsi_nama'>Nama Provinsi</label>
                                        <input
                                            id='provinsi_nama'
                                            type='text'
                                            className={`form-control ${this.hasErrorFor('provinsi_nama') ? 'is-invalid' : ''}`}
                                            name='provinsi_nama'
                                            value={provinsi_nama}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('provinsi_nama')}
                                    </div>
                                    <Link
                                        className='btn btn-secondary'
                                        to={`/provinsi`}
                                    >Back
                                    </Link>
                                    <button className='btn btn-primary'>Update</button>
                                    {this.state.alert}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ProvinsiEdit
