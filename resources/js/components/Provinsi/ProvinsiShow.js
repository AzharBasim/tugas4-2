import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class ProvinsiShow extends Component {
    constructor (props) {
        super(props)
        this.state = {
            provinsi: {}
        }
    }

    componentDidMount () {

        const provinsi_id = this.props.match.params.provinsi_id

        axios.get(`/api/provinsi/${provinsi_id}`).then(response => {
            this.setState({
                provinsi: response.data.data
            })
        })
    }

    render () {
        const { provinsi } = this.state
        console.log(provinsi.kabupaten)
        return (
            <>
                <div className='container py-4'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className='card'>
                                <div className='card-header'>Nama Provinsi: <b>{provinsi.provinsi_nama}</b></div>
                                <div className='card-body'>
                                    <p>{provinsi.provinsi_nama}</p>
                                    <Link
                                        className='btn btn-primary'
                                        to={`/provinsi`}
                                    >Back
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='container py-4'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className='card'>
                                <div className='card-header bg-dark text-white'>Semua Kabupaten pada Provinsi <b>{provinsi.provinsi_nama}</b></div>
                                <div className='card-body'>
                                    <Link className='btn btn-primary btn-sm mb-3' to='/provinsi/create'>
                                        Tambahkan Kabupaten Baru untuk <b>{provinsi.provinsi_nama}</b>
                                    </Link>
                                    <div className="table-responsive">
                                        <table className="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th width="50" className="text-center">#</th>
                                                <th>Nama Kabupaten</th>
                                                <th>Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            { provinsi.kabupaten?.map((kabupaten, i) => (
                                                <tr key={i}>
                                                    <td width="50" className="text-center">{i + 1}</td>
                                                    <td>{kabupaten.kabupaten_nama}</td>
                                                    <td width="200" className="text-center">
                                                        <div className="btn-group">
                                                            <Link
                                                                className='btn btn-primary btn-sm'
                                                                to={`/kabupaten/${kabupaten.kabupaten_id}`}
                                                            >Detail
                                                            </Link>
                                                            <Link
                                                                className='btn btn-success btn-sm'
                                                                to={`/kabupaten/edit/${kabupaten.kabupaten_id}`}
                                                            >Edit
                                                            </Link>
                                                            <button
                                                                className='btn btn-danger btn-sm'
                                                            >Hapus
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )) }
                                            </tbody>
                                        </table>
                                        {this.state.alert}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default ProvinsiShow
